#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

IMAGE="docker.io/debian:bookworm-slim"

echo "${IMAGE}"
