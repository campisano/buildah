#!/usr/bin/env bash

set -x -o errexit -o nounset -o pipefail

mkdir -m 777 -p ${CACHE_DIR} ${BUILD_DIR}

CONTAINER_IMAGE=$(./ci/custom/get_container_image_build.sh)
PROJECT_NAME=$(./ci/custom/get_project_name.sh)

# prepare a container from the defined image
CONTAINER=$(buildah from --tls-verify=false --pull=false "${CONTAINER_IMAGE}")



# build the code and run coverage isolatedly
buildah run \
       --volume "$(pwd)/${CACHE_DIR}:/srv/cache:rw" \
       --volume "$(pwd)/${BUILD_DIR}:/srv/build:ro" \
       --volume "$(pwd):/srv/repo_host:ro" \
       "${CONTAINER}" \
       /bin/bash -c \
       "export SONAR_ORGANIZATION='${SONAR_ORGANIZATION}' SONAR_TOKEN='${SONAR_TOKEN}' PROJECT_NAME='${PROJECT_NAME}'; cp -a /srv/repo_host /srv/repo; cd /srv/repo; ./ci/custom/internal_coverage.sh"
